<?php

/**
*	Implementation of hook_menu
*/
function s4_api_rest_menu() {
  $items = array();
  
  $items['admin/config/s4/rest-api'] = array(
    'title' => 'REST Integration',
    'description' => 'Configure REST integration with your Student Information System.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('s4_api_rest_admin_form'),
    'access arguments' => array('administer site configuration'),
    'weight' => 4
  );
  
  return $items;
}

/**
*	Admin form for the REST API
*/
function s4_api_rest_admin_form() {
  $form = array();
  
  $form['http_authentication'] = array(
    '#type' => 'fieldset',
    '#title' => 'HTTP Authentication',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['http_authentication']['s4_api_rest_http_authentication'] = array(
    '#type' => 'checkbox',
    '#title' => 'Use HTTP Authentication',
    '#default_value' => variable_get('s4_api_rest_http_authentication', FALSE),
  );
  
  $form['http_authentication']['s4_api_rest_http_authentication_username'] = array(
    '#type' => 'textfield',
    '#title' => 'HTTP Authentication username',
    '#default_value' => variable_get('s4_api_rest_http_authentication_username', FALSE),
  );
  
  $form['http_authentication']['s4_api_rest_http_authentication_password'] = array(
    '#type' => 'password',
    '#title' => 'HTTP Authentication password',
    '#default_value' => variable_get('s4_api_rest_http_authentication_password', FALSE),
  );
  
  $form['term'] = array(
    '#type' => 'fieldset',
    '#title' => 'Term',
  );
  
  $term = variable_get('s4_api_rest_term', array());
  
  $form['term']['term_url'] = array(
    '#type' => 'textfield',
    '#title' => 'URL to term callback',
    '#default_value' => $term['url'],
  );
  
  $form['term']['term_start_argument'] = array(
    '#type' => 'textfield',
    '#title' => 'Start date argument name',
    '#default_value' => $term['start_argument'],
  );
  
  $form['term']['term_end_argument'] = array(
    '#type' => 'textfield',
    '#title' => 'End date argument name',
    '#default_value' => $term['end_argument'],
  );
  
  $course = variable_get('s4_api_rest_course', array());
  
  $form['course'] = array(
    '#type' => 'fieldset',
    '#title' => 'Course',
  );
  
  $form['course']['course_url'] = array(
    '#type' => 'textfield',
    '#title' => 'URL to course callback',
    '#default_value' => $course['url'],
  );
  
  $form['course']['course_term_argument'] = array(
    '#type' => 'textfield',
    '#title' => 'Term code argument',
    '#default_value' => $course['term_argument'],
  );
  
  $user = variable_get('s4_api_rest_user', array());
  
  $form['user'] = array(
    '#type' => 'fieldset',
    '#title' => 'User',
  );
  
  $form['user']['user_url'] = array(
    '#type' => 'textfield',
    '#title' => 'URL to user callback',
    '#default_value' => $user['url'],
  );
  
  $form['user']['login_id_argument'] = array(
    '#type' => 'textfield',
    '#title' => 'Login ID argument',
    '#default_value' => $user['login_id_argument'],
  );
  
  $enrollment = variable_get('s4_api_rest_enrollment', array());
  
  $form['enrollment'] = array(
    '#type' => 'fieldset',
    '#title' => 'User enrollment',
  );
  
  $form['enrollment']['enrollment_url'] = array(
    '#type' => 'textfield',
    '#title' => 'URL to user enrollment callback',
    '#default_value' => $enrollment['url'],
  );
  
  $form['enrollment']['enrollment_user_id_argument'] = array(
    '#type' => 'textfield',
    '#title' => 'Login ID argument',
    '#default_value' => $enrollment['user_id_argument'],
  );
  
  $form['enrollment']['enrollment_term_argument'] = array(
    '#type' => 'textfield',
    '#title' => 'Term code argument',
    '#default_value' => $enrollment['term_argument'],
  );
  
  $faculty_courses = variable_get('s4_api_rest_faculty_courses', array());
  
  $form['faculty'] = array(
    '#type' => 'fieldset',
    '#title' => 'Faculty courses',
  );
  
  $form['faculty']['faculty_url'] = array(
    '#type' => 'textfield',
    '#title' => 'URL to faculty coursecallback',
    '#default_value' => $faculty_courses['url'],
  );
  
  $form['faculty']['faculty_user_id_argument'] = array(
    '#type' => 'textfield',
    '#title' => 'Login ID argument',
    '#default_value' => $faculty_courses['user_id_argument'],
  );
  
  $form['faculty']['faculty_term_argument'] = array(
    '#type' => 'textfield',
    '#title' => 'Term code argument',
    '#default_value' => $faculty_courses['term_argument'],
  );

  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save configuration',
  );
  
  return $form;
}

/**
*	Validation callback for the admin form
*/
function s4_api_rest_admin_form_validate($form, $form_state) {
  $url_fields = array('term_url', 'course_url');
  foreach ($url_fields as $url) {
    if (!valid_url($form_state['values'][$url], TRUE)) {
      form_set_error($url, t('This is not a valid URL: @url', 
                array('@url' => $form_state['values'][$url])));
    }
  }
}

/**
*	Submit handler for admin form
*/
function s4_api_rest_admin_form_submit($form, $form_state) {
  variable_set('s4_api_rest_http_authentication', 
         $form_state['values']['s4_api_rest_http_authentication']);
  variable_set('s4_api_rest_http_authentication_username', 
         $form_state['values']['s4_api_rest_http_authentication_username']);
  variable_set('s4_api_rest_http_authentication_password', 
         $form_state['values']['s4_api_rest_http_authentication_password']);
  
  
  $term = array(
    'url'        => $form_state['values']['term_url'],
    'start_argument' => $form_state['values']['term_start_argument'],
    'end_argument'    => $form_state['values']['term_end_argument'],
  );
  variable_set('s4_api_rest_term', $term);
  
  $course = array(
    'url'        => $form_state['values']['course_url'],
    'term_argument'  => $form_state['values']['course_term_argument'],
  );
  variable_set('s4_api_rest_course', $course);
  
  $user = array(
    'url'       => $form_state['values']['user_url'],
    'login_id_argument' => $form_state['values']['login_id_argument']
  );
  variable_set('s4_api_rest_user', $user);
  
  $enrollment = array(
    'url'       => $form_state['values']['enrollment_url'],
    'user_id_argument' => $form_state['values']['enrollment_user_id_argument'],
    'term_argument' => $form_state['values']['enrollment_term_argument'],
  );
  variable_set('s4_api_rest_enrollment', $enrollment);
  
  $faculty = array(
    'url'       => $form_state['values']['faculty_url'],
    'user_id_argument' => $form_state['values']['faculty_user_id_argument'],
    'term_argument' => $form_state['values']['faculty_term_argument'],
  );
  variable_set('s4_api_rest_faculty_courses', $faculty);
}

/**
*  Helper function. Returns the actual request
*/
function _s4_api_rest_get_data($url, $arguments) {
  if (!valid_url($url, TRUE)) {
    return FALSE;
  }
  $headers = array();
  if (variable_set('s4_api_rest_http_authentication', FALSE)) {
    $headers['Authorization'] = base64_encode(
      variable_get('s4_api_rest_http_authentication_username', '') . ':' . 
      variable_get('s4_api_rest_http_authentication_password', '')
    );
  }
  $result = drupal_http_request(url($url, array('query' => $arguments, 'headers' => $headers)));
  if ($result && $result->code == 200) {
    $xml = new SimpleXMLElement(str_replace('s4:', '', $result->data));
    return $xml;
  }
  return FALSE;
}

/**
*	Implementation of hook_s4_get_term
*/
function s4_api_rest_s4_get_term($start_date, $end_date) {
  $term = variable_get('s4_api_rest_term', array());
  $xml = _s4_api_rest_get_data($term['url'],
                  array($term['start_argument'] => $start_date,
                      $term['end_argument'] => $end_date));
  if (!$xml) {
    return FALSE;
  }
  $terms = array();
  foreach ($xml->entry as $entry) {
    $terms[] = array('term_title' => (string)$entry->title,
               'term_code'  => (string)$entry->id,
               'start_date' => date('Y-m-d', strtotime($entry->start_date)),
               'end_date'   => date('Y-m-d', strtotime($entry->end_date)),
    );
  }
  return $terms;
}

/**
*	Implementation of hook_s4_get_courses
*/
function s4_api_rest_s4_get_courses($term) {
  $course = variable_get('s4_api_rest_course', array());
  $xml = _s4_api_rest_get_data($course['url'],
                  array($course['course_term_argument'] => $term));
  if (!$xml) {
    return FALSE;
  }
  
  return _s4_api_rest_parse_courses($xml);
}

/**
*	Implementation of hook_s4_get_user_enrollment
*/
function s4_api_rest_s4_get_user_enrollment($user_id, $term) {
  $enrollment = variable_get('s4_api_rest_enrollment', array());
  $xml = _s4_api_rest_get_data($enrollment['url'],
                  array($enrollment['user_id_argument'] => $user_id,
                        $enrollment['term_argument'] => $term));
  if (!$xml) {
    return FALSE;
  }
  
  return _s4_api_rest_parse_courses($xml);
}

/**
*	Implementation of hook_s4_get_faculty_courses
*/
function s4_api_rest_s4_get_faculty_courses($user_id, $term) {
  $faculty = variable_get('s4_api_rest_faculty_courses', array());
  $xml = _s4_api_rest_get_data($faculty['url'],
                  array($faculty['user_id_argument'] => $user_id,
                        $faculty['term_argument'] => $term));
  if (!$xml) {
    return FALSE;
  }
  return _s4_api_rest_parse_courses($xml);
}

/**
*	Implementation of hook_s4_get_user
*/
function s4_api_rest_s4_get_user($login_id) {
  $user = variable_get('s4_api_rest_user', array());
  $xml = _s4_api_rest_get_data($user['url'],
                  array($user['login_id_argument'] => $login_id));
  if (!$xml) {
    return FALSE;
  }
  return array(
    'login_id'         => (string)$xml->entry->user->attributes()->{'login_id'},
    'user_id'         => (string)$xml->entry->user->attributes()->{'user_id'},
    'email'         => (string)$xml->entry->user->email,
    'first_name'       => (string)$xml->entry->user->name->attributes()->{'first_name'},
    'last_name'       => (string)$xml->entry->user->name->attributes()->{'last_name'},
    'restrictions'       => explode(',', (string)$xml->entry->user->restrictions),
    'constant_registration' => (property_exists(
                        $xml->entry->user->attributes(), 'constant_registration'))
                         ? (string)$xml->entry->user->attributes()->{'constant_registration'}
                         : 0,
  );
}

/**
*	Helper function to parse courses
*/
function _s4_api_rest_parse_courses($xml) {
  $courses = array();
  foreach ($xml->entry as $entry) {
    $status_attributes = $entry->status->attributes();
    $course = array(
      'subject'      => (string)$entry->subject,
      'catalog_number' => (string)$entry->catalog_number,
      'section'      => (string)$entry->section,
      'title'      => (string)$entry->title,
      'status'      => (string)$entry->status->attributes()->{'value'},
      'faculty'     => array(),
      'students'     => array(),
    );
    foreach (array('faculty', 'students') as $type) {
      if (!is_array($entry->{$type}->user)) {
        continue;
      }
      foreach ($entry->{$type}->user as $user_type) {
        $course[$type][] = array(
          'login_id'         => (string)$user_type->attributes()->{'login_id'},
            'user_id'         => (string)$user_type->attributes()->{'user_id'},
            'email'         => (string)$user_type->email,
            'first_name'       => (string)$user_type->name->attributes()->{'first_name'},
            'last_name'       => (string)$user_type->name->attributes()->{'last_name'},
            'restrictions'       => explode(',', (string)$user_type->restrictions),
            'constant_registration' => (property_exists(
                          $user_type->attributes(), 'constant_registration'))
                           ? $user_type->attributes()->{'constant_registration'}
                           : 0,
        );
      }
    }
    $courses[] = $course;
  }
  return $courses;
}